package com.yemagci.springelasticsearch.repository;

import com.yemagci.springelasticsearch.entity.Person;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface PersonRepository extends ElasticsearchRepository<Person,String> {
    
    List<Person> getByCustomQuery(String search);

}
