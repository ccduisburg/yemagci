package com.yemagci.springelasticsearch.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;
@Document(indexName = "people",type="person")
@Getter
@Setter
public class Person {
    @Id
    private String id;
    @Field(name="vorname", type= FieldType.Text)
    private String vorname;
    @Field(name="nachname", type= FieldType.Text)
    private String nachname;
    @Field(name="birthday", type= FieldType.Date)
    private Date birthday;
    @Field(name="adres", type= FieldType.Text)
    private String adres;

    public Person(String id, String vorname, String nachname, Date birthday, String adres) {
        this.id = id;
        this.vorname = vorname;
        this.nachname = nachname;
        this.birthday = birthday;
        this.adres = adres;
    }

    public Person() {
    }

    @Override
    public String toString() {
        return "Person{" +
                "id='" + id + '\'' +
                ", vorname='" + vorname + '\'' +
                ", nachname='" + nachname + '\'' +
                ", birthday=" + birthday +
                ", adres='" + adres + '\'' +
                '}';
    }
}
