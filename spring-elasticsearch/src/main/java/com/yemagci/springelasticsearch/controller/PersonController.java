package com.yemagci.springelasticsearch.controller;

import com.yemagci.springelasticsearch.entity.Person;
import com.yemagci.springelasticsearch.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/person")
public class PersonController {
private PersonRepository personRepository;
@GetMapping("/{search}")
public ResponseEntity<List<Person>> getPersonal(@PathVariable String search){
     List<Person> personal =personRepository.getByCustomQuery(search);
     return ResponseEntity.ok(personal);
}
}
