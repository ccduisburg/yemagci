package com.yemagci.repository;

import com.yemagci.entitty.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User,String> {
}
