package com.yemagci;

import com.yemagci.entitty.User;
import com.yemagci.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserRepository userRepository;
    @PostConstruct
    public void init(){

        userRepository.save(new User("cc","kk"));
        userRepository.save(new User("cm","km"));
        userRepository.save(new User("cn","kn"));

        User usr1=new User();
        usr1.setId("111");
        usr1.setNachname("kabakci");
        usr1.setVorname("cemil");
        
        User usr2=new User();
        usr2.setId("111");
        usr2.setNachname("kabakci");
        usr2.setVorname("cemil");
        User usr3=new User();
        usr3.setId("111");
        usr3.setNachname("kabakci");
        usr3.setVorname("cemil");

    }

    @PostMapping
    public ResponseEntity<User> add(@RequestBody User user) {
        return ResponseEntity.ok(userRepository.save(user));
    }

    @GetMapping("/users")
    public ResponseEntity<List<User>> getAllUser() {
        return ResponseEntity.ok(userRepository.findAll());
    }
}

