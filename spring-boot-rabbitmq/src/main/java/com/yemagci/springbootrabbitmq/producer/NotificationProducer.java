package com.yemagci.springbootrabbitmq.producer;

import com.yemagci.springbootrabbitmq.model.Notification;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.UUID;

@Service
public class NotificationProducer {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Value("${sr.rabbitmq.routing.name}")
    private String routingName;

    @PostConstruct
    public void init(){
        Notification notification=new Notification();
        notification.setNotificationId(UUID.randomUUID().toString());
        notification.setCreateDate(new Date());
        notification.setMessage("Wilkommen our rabbitMq");
        notification.setSeen(Boolean.FALSE);
        sedToQueue(notification);
    }

    public void sedToQueue(Notification notification){
        System.out.println("Notification Sent ID: "+notification.getNotificationId());
        rabbitTemplate.convertAndSend(routingName,notification);
    }
}
