package com.yemagci.springbootrabbitmq.model;

import lombok.*;

import java.io.Serializable;
import java.util.Date;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Notification implements Serializable {
    private String notificationId;
    private Date createDate;
    private Boolean seen;
    private String message;

}
