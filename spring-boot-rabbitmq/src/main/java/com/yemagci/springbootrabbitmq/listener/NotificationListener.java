package com.yemagci.springbootrabbitmq.listener;

import com.yemagci.springbootrabbitmq.model.Notification;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class NotificationListener {
    @RabbitListener(queues = "yemagci-queue")//kuyruk ismini properties dosyasindan aliyoruz. fakat automatik @Valu ile de alabilirdik
    public void handleMEssage(Notification notification){
        System.out.println("mesaj alindi");
        System.out.println(notification.toString());
    }
}
