package com.yemagci.springpostgresqlrestapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Arrays;
import java.util.Collections;

import com.yemagci.springpostgresqlrestapi.dto.KisiDto;
import com.yemagci.springpostgresqlrestapi.service.Kisiservice;
import com.yemagci.springpostgresqlrestapi.service.impl.KisiServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
@WebMvcTest(controllers = KisiController.class)
public class KisiControllerTest {

    private final static String CONTENT_TYPE = "application/json";


    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private KisiServiceImpl kisiservice;

    @Test//kaydet i test ediyoruz
    void whenValidInput_thenReturns200() throws Exception{
        //given
        KisiDto kisi= KisiDto.builder().adi("cc").soyadi("kk").build();
        //when
        ResultActions actions = mockMvc.perform(post("/kisi")
                //bu method json type kabul ediyor
                .contentType(CONTENT_TYPE)
                //java nesnesini Json a ceviriyor
                .content(objectMapper.writeValueAsString(kisi)));

        //then
        //objenin o anki state ini yakalamak icin kullaniliyor
        ArgumentCaptor<KisiDto> captor=ArgumentCaptor.forClass(KisiDto.class);
        //save methodu 1 kere cagrildigindan emin ol ayrica save methoduna giden parametreyi de yakaliyor
        verify(kisiservice, times(1)).save(captor.capture());
        assertThat(captor.getValue().getAdi()).isEqualTo("cc");
        assertThat(captor.getValue().getSoyadi()).isEqualTo("kk");
        actions.andExpect(status().isOk());
    }

    @Test
    void whenValidInput_thenReturns400() throws Exception {
        // given

        // when
        ResultActions actions = mockMvc.perform(post("/kisi")
                .contentType(CONTENT_TYPE)
                //kisi adresine rastgete test gönderiyorum bekledigim sey basariz bir islem
                .content(objectMapper.writeValueAsString("test-value")));

        // then
        actions.andExpect(status().isBadRequest());
    }

    @Test//javax validation testi @NotNull test ediyorum
    // bu testi yapabilmek icin controller da kaydet(@Valid @RequestBody KisiDto kisiDto) @valid anotation olmasi gerekir
    void whenInvalidInput_thenReturns400() throws Exception {
        // given
        KisiDto kisi = KisiDto.builder().soyadi("kk").build();

        // when
        ResultActions actions = mockMvc.perform(post("/kisi")
                .contentType(CONTENT_TYPE)
                .content(objectMapper.writeValueAsString(kisi)));

        // then
        actions.andExpect(status().isBadRequest());
    }

    @Test
    void whenCallTumunuListele_thenReturns200() throws Exception {
        // given
        KisiDto kisi = KisiDto.builder().adi("cc").soyadi("kk").build();
        when(kisiservice.getAll()).thenReturn(Arrays.asList(kisi));

        // when
        MvcResult mvcResult = mockMvc.perform(get("/kisi")
                .accept(CONTENT_TYPE)).andReturn();


        // then
        String responseBody = mvcResult.getResponse().getContentAsString();
        verify(kisiservice, times(1)).getAll();
        assertThat(objectMapper.writeValueAsString(Arrays.asList(kisi)))
                .isEqualToIgnoringWhitespace(responseBody);
    }

    @Test
    void whenCallTumunuListele_thenReturnsNoData() throws Exception {
        // given
        when(kisiservice.getAll()).thenReturn(Collections.emptyList());

        // when
        MvcResult mvcResult = mockMvc.perform(get("/kisi")
                .accept(CONTENT_TYPE)).andReturn();

        // then
        String responseBody = mvcResult.getResponse().getContentAsString();
        verify(kisiservice, times(1)).getAll();
        assertThat(objectMapper.writeValueAsString(Collections.emptyList()))
                .isEqualToIgnoringWhitespace(responseBody);
    }
}