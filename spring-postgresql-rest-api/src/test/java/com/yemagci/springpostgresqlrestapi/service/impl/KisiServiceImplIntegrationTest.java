package com.yemagci.springpostgresqlrestapi.service.impl;

import com.yemagci.springpostgresqlrestapi.dto.KisiDto;

import com.yemagci.springpostgresqlrestapi.entitty.Adres;
import com.yemagci.springpostgresqlrestapi.repo.AdresRepository;
import com.yemagci.springpostgresqlrestapi.service.Kisiservice;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class KisiServiceImplIntegrationTest {
    @Autowired
    private Kisiservice kisiservice;
    @Autowired
    private AdresRepository adresRepository;
    @Test
    public void testSave() {
        KisiDto kisiDto = new KisiDto();
        kisiDto.setAdi("Test-Name");
        kisiDto.setSoyadi("Test-Lastname");
        kisiDto.setAdres(Arrays.asList("Test-Adres-1"));

        KisiDto result = kisiservice.save(kisiDto);
        List<Adres> listevonAdres=adresRepository.findAll();
        assertTrue(result.getId()>0L);
       assertEquals(listevonAdres.size(),1);
    }

}
