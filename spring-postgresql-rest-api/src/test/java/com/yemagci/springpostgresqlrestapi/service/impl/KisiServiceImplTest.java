package com.yemagci.springpostgresqlrestapi.service.impl;

import com.yemagci.springpostgresqlrestapi.dto.KisiDto;
import com.yemagci.springpostgresqlrestapi.entitty.Adres;
import com.yemagci.springpostgresqlrestapi.entitty.Kisi;
import com.yemagci.springpostgresqlrestapi.repo.AdresRepository;
import com.yemagci.springpostgresqlrestapi.repo.KisiRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class KisiServiceImplTest {
    @InjectMocks
    private KisiServiceImpl kisiServiceimp;

    @Mock
    private KisiRepository kisiRepository;
    @Mock
    private AdresRepository adresRepository;

    @Test
    public void testSave() {
        KisiDto kisiDto = new KisiDto();
        kisiDto.setAdi("Test-Name");
        kisiDto.setSoyadi("Test-Lastname");
        kisiDto.setAdres(Arrays.asList("Test-Adres-1"));
        Kisi kisiMock = mock(Kisi.class);

        when(kisiMock.getId()).thenReturn(1L);
        when(kisiRepository.save(ArgumentMatchers.any(Kisi.class))).thenReturn(kisiMock);

        KisiDto result = kisiServiceimp.save(kisiDto);

        assertEquals(result.getAdi(), kisiDto.getAdi());
        assertEquals(result.getId(), 1L);
    }
    @Test
    public void testSaveException() {
        KisiDto kisiDto = new KisiDto();
        kisiDto.setSoyadi("Test-Name");//setAdi yerine baska field set ettik cünkü exception bekliyoruz
        kisiDto.setSoyadi("Test-Lastname");
        kisiDto.setAdres(Arrays.asList("Test-Adres-1"));
        assertThrows(IllegalArgumentException.class,()-> kisiServiceimp.save(kisiDto));

    }
    @Test
    public void delete() {
    }

    @Test
    public void testgetAll() {
        Kisi kisi = new Kisi();
        kisi.setId(1L);//unit test oldugu icin veri tabani islemi yaptirmamaliyiz
        kisi.setAdi("Test-Name");
        kisi.setSoyadi("Test-Lastname");
        when(kisiRepository.findAll()).thenReturn(Collections.singletonList(kisi));
        List<KisiDto> kisiDtos=kisiServiceimp.getAll();
        assertEquals(kisiDtos.size(),1);
        //Iki nesne birbirine esit olup olmadigini test ediyoruz
        assertEquals(kisiDtos.get(0),KisiDto.builder().id(1L).build());
    }

    @Test
    public void testgetAllWithAdres() {
        Kisi kisi = new Kisi();
        kisi.setId(1L);//unit test oldugu icin veri tabani islemi yaptirmamaliyiz
        kisi.setAdi("Test-Name");
        kisi.setSoyadi("Test-Lastname");
        Adres adres=new Adres();
        adres.setAdresType(Adres.AdresType.DIGER);
        adres.setAdres("Test Adres");
        kisi.setAdres(Collections.singletonList(adres));

        when(kisiRepository.findAll()).thenReturn(Collections.singletonList(kisi));
        List<KisiDto> kisiDtos=kisiServiceimp.getAll();


        assertEquals(kisiDtos.get(0).getAdres().size(),1);
    }

}