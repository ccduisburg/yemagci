package com.yemagci.springpostgresqlrestapi.service;

import com.yemagci.springpostgresqlrestapi.dto.KisiDto;
import org.springframework.data.domain.Page;

import java.awt.print.Pageable;
import java.util.List;

public interface Kisiservice {
    KisiDto save(KisiDto kisiDto);
    void delete(Long id);
    List<KisiDto> getAll();
    Page<KisiDto> getAll(Pageable pegeable);
}
