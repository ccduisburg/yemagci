package com.yemagci.springpostgresqlrestapi.service.impl;

import com.yemagci.springpostgresqlrestapi.dto.KisiDto;
import com.yemagci.springpostgresqlrestapi.entitty.Adres;
import com.yemagci.springpostgresqlrestapi.entitty.Kisi;
import com.yemagci.springpostgresqlrestapi.repo.AdresRepository;
import com.yemagci.springpostgresqlrestapi.repo.KisiRepository;
import com.yemagci.springpostgresqlrestapi.service.Kisiservice;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.awt.print.Pageable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service

public class KisiServiceImpl implements Kisiservice {

    private final KisiRepository kisiRepository;
    private final AdresRepository adresRepository;

    public KisiServiceImpl(KisiRepository kisiRepository, AdresRepository adresRepository) {
        this.kisiRepository = kisiRepository;
        this.adresRepository = adresRepository;
    }

    @Override
    @Transactional
    public KisiDto save(KisiDto kisiDto) {
        Assert.notNull(kisiDto.getAdi(),"Adi alani zorunludur!");

       final Kisi kisi=new Kisi();
        kisi.setAdi(kisiDto.getAdi());
        kisi.setSoyadi(kisiDto.getSoyadi());
        final Kisi kisiDb = kisiRepository.save(kisi);
        List<Adres> liste=new ArrayList<>();
        kisiDto.getAdres().forEach(item->{
           Adres adres=new Adres();
           adres.setAdres(item);
           adres.setAdresType(Adres.AdresType.DIGER);
           adres.setAktif(true);
           adres.setKisi(kisiDb);
           liste.add(adres);

        });
        adresRepository.saveAll(liste);
        kisiDto.setId(kisiDb.getId());
        return kisiDto;
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public List<KisiDto> getAll() {
        List<Kisi> kisiler =kisiRepository.findAll();
        List<KisiDto> kisiDtos=new ArrayList<>();
        kisiler.forEach(item->{
            KisiDto kisiDto=new KisiDto();
            kisiDto.setId(item.getId());
            kisiDto.setAdi(item.getAdi());
            kisiDto.setSoyadi(item.getSoyadi());
            kisiDto.setAdres(
                    item.getAdres()!=null?item.getAdres().stream().map(Adres::getAdres).collect(Collectors.toList()):null);
            kisiDtos.add(kisiDto);
                            });
        return kisiDtos;
    }

    @Override
    public Page<KisiDto> getAll(Pageable pegeable) {
        return null;
    }
}
