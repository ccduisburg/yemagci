package com.yemagci.springpostgresqlrestapi.controller;

import com.yemagci.springpostgresqlrestapi.dto.KisiDto;
import com.yemagci.springpostgresqlrestapi.service.Kisiservice;
import com.yemagci.springpostgresqlrestapi.service.impl.KisiServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/kisi")
@RequiredArgsConstructor
public class KisiController {
    private final KisiServiceImpl kisiservice;
    @PostMapping
    public ResponseEntity<KisiDto> kaydet(@Valid @RequestBody KisiDto kisiDto){
        return ResponseEntity.ok(kisiservice.save(kisiDto));
    }
    @GetMapping
    public ResponseEntity<List<KisiDto>> hepsinilistele(){
        return ResponseEntity.ok(kisiservice.getAll());
    }
}
