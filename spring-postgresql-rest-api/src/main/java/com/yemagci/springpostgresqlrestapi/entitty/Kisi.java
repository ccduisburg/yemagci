package com.yemagci.springpostgresqlrestapi.entitty;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name="kisi")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
@ToString
public class Kisi implements Serializable {
    @Id
    @SequenceGenerator(name="seq_kisi" ,allocationSize = 1)
    @GeneratedValue(generator = "seq_kisi",strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(length = 100,name = "ad")
    private String adi;
    @Column(length = 100,name = "soyadi")
    private String soyadi;
    @OneToMany
    @JoinColumn(name = "kisi_adres_id")
    private List<Adres> adres;
}
