package com.yemagci.springpostgresqlrestapi.entitty;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="kisiAdres")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})// iki nesnenin id leri birbirine esit ise bu iki nesne esittir karsilastirmasi icin ekledik
public class Adres implements Serializable {
    @Id
    @SequenceGenerator(name="seg_kisi_adres",allocationSize = 1)// er birer artmasi icin normalde 50 ser 50 ser artiyor
    @GeneratedValue(generator = "seg_kisi_adres",strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(length = 200,name = "adres")
    private String adres;
    @Enumerated
    private AdresType adresType;
    @Column(name = "aktif")
    private Boolean aktif;
    @ManyToOne//Veri tabanindan her adres get yapildigin da kisi bilgisi de gelsin
    @JoinColumn(name="kisi_adres_id")
    private Kisi kisi;
    public enum AdresType{
        EV_ADRESI,
        IS_ADRESI,
        DIGER
    }
}
