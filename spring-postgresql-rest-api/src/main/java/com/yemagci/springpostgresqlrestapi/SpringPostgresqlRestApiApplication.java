package com.yemagci.springpostgresqlrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
//jpa repository lerin instance yi olusturacak ve install yapacak
//@EnableJpaRepositories
public class SpringPostgresqlRestApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringPostgresqlRestApiApplication.class, args);
    }

}
