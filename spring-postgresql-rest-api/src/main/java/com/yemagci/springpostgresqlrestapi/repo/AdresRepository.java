package com.yemagci.springpostgresqlrestapi.repo;

import com.yemagci.springpostgresqlrestapi.entitty.Adres;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


public interface AdresRepository extends JpaRepository<Adres,Long> {
}
