package com.yemagci.springpostgresqlrestapi.repo;

import com.yemagci.springpostgresqlrestapi.entitty.Kisi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


public interface KisiRepository extends JpaRepository<Kisi,Long> {
    //Kisi save(Kisi kisi);

}
